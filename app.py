# coding=utf-8

import threading
import httplib, urllib
import os
import json
import sys
from flask import Flask, render_template, request, redirect, url_for
import re
import datetime
import traceback

reload(sys)
sys.setdefaultencoding('utf8')

months = "(january|jan|february|feb|march|mar|april|apr|may|june|july|august|aug|september|sept|october|oct|november|nov|december|dec)"
days = "(monday|tuesday|wednesday|thursday|friday|saturday|sunday)"

# PERSON COUNT #

people_pattern = re.compile("\\b(\d+) (?:adult|person|people|man|woman|men|women|child|boy|girl|kid|bab(?:y|ies))s?\\b", re.IGNORECASE)


# DATE #

# DD~MM~YYYY
full_date_pattern = re.compile("\\b(\d{1,2})(?P<sp>[-/.\\\])(\d{1,2})(?P=sp)(\d{4})\\b", re.IGNORECASE)

near_date_pattern = re.compile("\\b(today|tomorrow|this month|next month|this year|next year)\\b", re.IGNORECASE)

# 3 days later
relative_date_pattern = re.compile("\\b(\d+) (hour|day|month|year)s? later\\b", re.IGNORECASE)

# 3rd of september ((,) 2017)
day_and_month_pattern = re.compile("\\b(\d+) ?(?:st|rd|nd|th) (?:of )?" + months + " *,? *(\d{4})?\\b", re.IGNORECASE)

# september (the) 3rd ((,) 2017)
day_and_month_pattern_2 = re.compile("\\b" + months + "(?: the)? +(\d+) ?(?:st|rd|nd|th) *,? *(\d{4})?\\b", re.IGNORECASE)

# in september ((,) 2017)
month_pattern = re.compile("\\bin +" + months + " *,? *(\d{4})?\\b", re.IGNORECASE)

# in 2017
year_pattern = re.compile("\\bin +(\d{4})\\b")


# ROUTE #

# IST->ESB
route_pattern = re.compile("\\b(\w+) *-> *(\w+)\\b", re.IGNORECASE)

# from istanbul(, turkey)
from_pattern = re.compile("\\bfrom +([a-zA-Z ]{3,})(, *[\w ]{3,})?\\b", re.IGNORECASE)

# origin: istanbul(, turkey)
from_pattern_2 = re.compile("\\borigin: *([a-zA-Z ]{3,})(, *[\w ]{3,})?\\b", re.IGNORECASE)

# to istanbul(, turkey)
to_pattern = re.compile("(?<!want |wish |like )\\bto +([\w ]{3,})(, *[a-zA-Z ]{3,})?\\b", re.IGNORECASE)

# dest(ination): istanbul(, turkey)
to_pattern_2 = re.compile("\\bdest(?:ination)?: *([a-zA-Z ]{3,})(, *[\w ]{3,})?\\b", re.IGNORECASE)

# boundary for routes
route_boundary_pattern = re.compile(months + "|\\d|,|\\.|\\b(from|orig(in)?|to|dest(ination)?|in|on|at|with|for)\\b", re.IGNORECASE)

queries = {}
app = Flask(__name__)
access_token = "EAABdZBZATuNuEBAKo55k3f31eXZAFVYrzOjNkBOZAZCc8UlU1AjCy6bJEltajHMBG5FAhmrZCcFnLNLGQWPcJx64i9rAhFdpyjLI" \
               "ZALxezQR0WQjRLsq5ZAqKqrZApzOgnrod25KXXLp5EQKZC00hshQrPW4YiZAwCvdJTDKCqMeVopRgZDZD"


class Status:
    normal = 1
    dest_clarification = 2
    orig_clarification = 3


class Place:
    def __init__(self, place_id, place_name, country_name):
        self.place_id = place_id
        self.place_name = place_name
        self.country_name = country_name


class Query:
    def __init__(self):
        self.route_from = ""
        self.route_to = ""
        self.day = ""
        self.month = ""
        self.year = ""
        self.person_count = ""
        self.clarify_choices = []
        self.status = Status.normal
        self.from_clear = False
        self.to_clear = False

    def all_valid(self):
        return self.route_from != "" and self.route_to != "" and self.day != "" \
               and self.month != "" and self.year != "" and self.person_count != ""

    def required_fields(self):
        ret = ""
        if self.route_from == "":
            ret += " origin,"
        if self.route_to == "":
            ret += " destination,"
        if self.day == "":
            ret += " day,"
        if self.month == "":
            ret += " month,"
        if self.year == "":
            ret += " year,"
        if self.person_count == "":
            ret += " number of people traveling,"
        return ret[:-1]
    
    def print_info(self):
        print("from: " + self.route_from + "; to: " + self.route_to + "; date: " + self.day + "." + self.month + "." + self.year + "; person count: " + self.person_count)


class Quote:
    def __init__(self, origin, destination, carrier_name, price, direct):
        self.origin = origin
        self.destination = destination
        self.price = price
        self.carrier_name = carrier_name
        self.direct = direct


def month_name_to_num(month):
    month = month.upper()
    if month.startswith("JAN"):
        return "1"
    elif month.startswith("FEB"):
        return "2"
    elif month.startswith("MAR"):
        return "3"
    elif month.startswith("APR"):
        return "4"
    elif month.startswith("MAY"):
        return "5"
    elif month.startswith("JUNE"):
        return "6"
    elif month.startswith("JULY"):
        return "7"
    elif month.startswith("AUG"):
        return "8"
    elif month.startswith("SEPT"):
        return "9"
    elif month.startswith("OCT"):
        return "10"
    elif month.startswith("NOV"):
        return "11"
    elif month.startswith("DEC"):
        return "12"
    else:
        print("invalid month: " + month)
        raise Exception


def valid_date(d="01", m="01", y="2017"):
    try:
        datetime.datetime.strptime(d + "." + m + "." + y, "%d.%m.%Y")
        return True
    except:
        return False


def parse_info(query, text):
    response = ""
    if query.status == Status.normal:
        if query.required_fields() == " year":      # only year is unknown => the user may have only entered the year
            m = re.search("\\b(\d{4})\\b", text)
            if m:
                query.year = m.group(1)
        elif query.required_fields() == " number of people traveling":
            m = re.match("\d+", text)
            if m:
                query.person_count = m.group(0)
        else:
            people_count = -1
            for num in people_pattern.findall(text):
                people_count += int(num)
            if people_count != -1:
                query.person_count = str(people_count + 1)

            m = full_date_pattern.search(text)
            if m:
                date_valid = True
                try:
                    sp = m.group(2)
                    if datetime.datetime.now() >= datetime.datetime.strptime(m.group(0), "%d" + sp + "%m" + sp + "%Y"):
                        date_valid = False
                except:
                    date_valid = True

                if date_valid:
                    query.day = m.group(1)
                    query.month = m.group(3)
                    query.year = m.group(4)
                else:
                    response += "\nInvalid date specified. dd/mm/yyyy format is accepted."

            m = near_date_pattern.search(text)
            if m:
                dt = datetime.datetime.now()
                captured = m.group(0).lower()
                if captured == "today":
                    query.day = str(dt.day)
                    query.month = str(dt.month)
                elif captured == "tomorrow":
                    dt += datetime.timedelta(days=1)
                    query.day = str(dt.day)
                    query.month = str(dt.month)
                elif captured == "this month":
                    query.month = str(dt.month)
                elif captured == "next month":
                    dt += datetime.timedelta(days=30)
                    query.month = str(dt.month)
                elif captured == "this year":
                    pass
                elif captured == "next year":
                    dt += datetime.timedelta(days=360)
                    pass
                query.year = str(dt.year)

            m = relative_date_pattern.search(text)
            if m:
                dt = datetime.datetime.now()
                count = int(m.group(1))
                captured = m.group(2).lower()
                if captured == "hour":
                    dt += datetime.timedelta(hours=count)
                elif captured == "day":
                    dt += datetime.timedelta(days=count)
                elif captured == "month":
                    dt += datetime.timedelta(days=30*count)
                elif captured == "year":
                    dt += datetime.timedelta(days=360*count)
                query.day = str(dt.day)
                query.month = str(dt.month)
                query.year = str(dt.year)

            m = day_and_month_pattern.search(text)
            if m:
                if valid_date(d=m.group(1), m=month_name_to_num(m.group(2)), y=m.group(3) if m.group(3) else "2017"):
                    query.day = m.group(1)
                    query.month = month_name_to_num(m.group(2))
                    if m.group(3):
                        query.year = m.group(3)
                else:
                    response += "\nInvalid date specified."

            m = day_and_month_pattern_2.search(text)
            if m:
                if valid_date(m=month_name_to_num(m.group(1)), d=m.group(2), y=m.group(3) if m.group(3) else "2017"):
                    query.month = month_name_to_num(m.group(1))
                    query.day = m.group(2)
                    if m.group(3):
                        query.year = m.group(3)
                else:
                    response += "\nInvalid date specified."

            m = month_pattern.search(text)
            if m:
                if valid_date(m=month_name_to_num(m.group(1)), y=m.group(2) if m.group(3) else "2017"):
                    query.month = month_name_to_num(m.group(1))
                    if m.group(2):
                        query.year = m.group(2)
                else:
                    response += "\nInvalid date specified."

            m = year_pattern.search(text)
            if m:
                if valid_date(y=m.group(1)):
                    query.year = m.group(1)
                else:
                    response += "\nInvalid date specified."

            m = route_pattern.search(text)
            if m:
                query.route_from = m.group(1)
                query.route_to = m.group(2)

            m = from_pattern.search(text)
            if m:
                m2 = route_boundary_pattern.search(m.group(1) + (m.group(2) or ""))
                if m2:
                    if m.group(2) is None or m2.start() < len(m.group(1)):
                        query.route_from = m.group(1)[:m2.start()]
                    else:
                        query.route_from = m.group(1) + m.group(2)[:m2.start()-len(m.group(1))]
                else:
                    query.route_from = m.group(1) + (m.group(2) or "")

            m = from_pattern_2.search(text)
            if m:
                m2 = route_boundary_pattern.search(m.group(1) + (m.group(2) or ""))
                if m2:
                    if m.group(2) is None or m2.start() < len(m.group(1)):
                        query.route_from = m.group(1)[:m2.start()]
                    else:
                        query.route_from = m.group(1) + m.group(2)[:m2.start()-len(m.group(1))]
                else:
                    query.route_from = m.group(1) + (m.group(2) or "")

            m = to_pattern.search(text)
            if m:
                m2 = route_boundary_pattern.search(m.group(1) + (m.group(2) or ""))
                if m2:
                    if m.group(2) is None or m2.start() < len(m.group(1)):
                        query.route_to = m.group(1)[:m2.start()]
                    else:
                        query.route_to = m.group(1) + m.group(2)[:m2.start()-len(m.group(1))]
                else:
                    query.route_to = m.group(1) + (m.group(2) or "")

            m = to_pattern_2.search(text)
            if m:
                m2 = route_boundary_pattern.search(m.group(1) + (m.group(2) or ""))
                if m2:
                    if m.group(2) is None or m2.start() < len(m.group(1)):
                        query.route_to = m.group(1)[:m2.start()]
                    else:
                        query.route_to = m.group(1) + m.group(2)[:m2.start()-len(m.group(1))]
                else:
                    query.route_to = m.group(1) + (m.group(2) or "")

            query.route_from = query.route_from.strip()
            query.route_to = query.route_to.strip()

            if query.day != "" and query.month != "" and query.year != "" and \
                            datetime.datetime.now() >= datetime.datetime.strptime(query.day + "." + query.month + "." + query.year, "%d.%m.%Y"):
                query.day = ""
                query.month = ""
                query.year = ""
                response += "\nYou have specified a date in the past. Please specify it again."

            if response != "":
                response = "Following errors occured:" + response

            return response
    elif query.status == Status.dest_clarification or query.status == Status.orig_clarification:
        try:
            index = int(text)
            if index < 1 or index > len(query.clarify_choices):
                raise Exception
            
            if query.status == Status.dest_clarification:
                query.route_to = query.clarify_choices[index-1].place_id
                query.to_clear = True
            else:
                query.route_from = query.clarify_choices[index-1].place_id
                query.from_clear = True
            
            query.status = Status.normal
            
        except:
            return "Your choice was invalid. Please specify a value between 1 and " + str(len(query.clarify_choices))


@app.route('/')
def home():
    return "<p>Flight bot created by Ömer Faruk Doğan, Gülşah Damla & Aziza Kasenova</p>"


@app.route('/message', methods=["GET"])
def verification():
    if request.args.get("hub.mode") == "subscribe" \
            and request.args.get("hub.verify_token") == "flight_bot_verification_authorized_request":
        return request.args.get("hub.challenge"), 200
    else:
        return "", 401
    
@app.route('/privacy', methods=["GET"])
def privacy_policy():
    return """<h3>Privacy Policy</h3>
            <p>Any personal information will not be collected, stored or shared.
            <br> All information are completely removed from the system after the query is concluded and flights are listed.</p>""", 200


@app.route('/message', methods=["POST"])
def message_received():
    data = json.loads(request.get_data())
    if data["object"] == "page":
        for entry in data["entry"]:
            # page_id = entry["id"]
            # time = entry["time"]
            for event in entry["messaging"]:
                if "message" in event:
                    threading.Thread(target=received_message, args=(event,)).start()
        return "", 200


def received_message(event):
    global queries
    sender_id = event["sender"]["id"]
    # recipient_id = event["recipient"]["id"]
    # time = event["timestamp"]
    message = event["message"]
    
    # message_id = message["mid"]
    message_text = message["text"]

    if sender_id in queries:
        query = queries[sender_id]
        if re.search("\\bcancel\\b", message_text):
            del queries[sender_id]
            send_message(sender_id, "Your query is cancelled.")
            return
    else:
        query = Query()
        
    response = parse_info(query, message_text)
    queries[sender_id] = query

    if query.all_valid():
        if not query.from_clear or not query.to_clear:
            searching = query.route_from if not query.from_clear else query.route_to
            headers = {"Accept": "application/json"}
            query_str = urllib.urlencode({"query": searching, "apiKey": "fl216856820901462413134628934571"})
            conn = httplib.HTTPConnection("partners.api.skyscanner.net")
            conn.request("GET", "/apiservices/autosuggest/v1.0/US/USD/en-US/?" + query_str, "", headers)
            places = json.loads(conn.getresponse().read())
            query.clarify_choices = []
            
            if len(places["Places"]) == 0:
                if not query.from_clear:
                    result = "Origin city could not be found."
                    query.route_from = ""
                else:
                    result = "Destination city could not be found."
                    query.route_to = ""
                    
                query.from_clear = False
                query.to_clear = False
            else:
                if not query.from_clear:
                    result = "Please choose one of the following places as the origin:"
                    query.status = Status.orig_clarification
                else:
                    result = "Please choose one of the following places as the destination:"
                    query.status = Status.dest_clarification
                    
                i = 1
                for place in places["Places"][:5]:
                    query.clarify_choices.append(Place(place["PlaceId"], place["PlaceName"], place["CountryName"]))
                    result += "\n" + str(i) + ". " + place["PlaceName"] + ", " + place["CountryName"]
                    i += 1
                
            send_message(sender_id, result)
        else:
            resp = ""
            try:
                headers = {"Accept": "application/json"}
                query_str = urllib.urlencode({"apiKey": "fl216856820901462413134628934571" })
                path = "/apiservices/browsequotes/v1.0/US/USD/en-US/{}/{}/{}-{}-{}/?" + query_str
                conn = httplib.HTTPConnection("partners.api.skyscanner.net")
                conn.request("GET", path.format(query.route_from, query.route_to, query.year,
                                                str(query.month).zfill(2), str(query.day).zfill(2)),
                            "", headers)
                resp = json.loads(conn.getresponse().read())
                quotes = []
                carriers = {}
                places = {}

                carriers_exist = False
                if "Carriers" in resp:
                    carriers_exist = True

                if carriers_exist:
                    for carrier in resp["Carriers"]:
                        carriers[carrier["CarrierId"]] = carrier["Name"]

                for place in resp["Places"]:
                    places[place["PlaceId"]] = (place["Name"], place["CityName"], place["CountryName"])

                for quote in resp["Quotes"][:10]:
                    quotes.append(Quote(places[quote["OutboundLeg"]["OriginId"]], places[quote["OutboundLeg"]["DestinationId"]],
                                        (carriers[quote["OutboundLeg"]["CarrierIds"][0]] if carriers_exist else ""),
                                        int(query.person_count) * quote["MinPrice"], quote["Direct"]))

                if len(quotes) != 0:
                    quotes = sorted(quotes, key=lambda quote: quote.price)
                    send_message(sender_id, "Here are the " + str(len(quotes)) + " cheapest flights from " +
                                 query.route_from + " to " + query.route_to + " at " +
                                 "{}/{}/{}".format(str(query.day).zfill(2), str(query.month).zfill(2), query.year) +
                                 " for " + str(query.person_count) + " people:")
                    for quote in quotes:
                        message = ("$" + str(quote.price) + " – " + ("Direct" if quote.direct else "Indirect")) + " flight" + \
                                " from " + quote.origin[0] + ", " + quote.origin[1] + ", " + quote.origin[2] + \
                                " to " + quote.destination[0] + ", " + quote.destination[1] + ", " + quote.destination[2] + \
                                (" with " + quote.carrier_name if carriers_exist else "")
                        send_message(sender_id, message)
                else:
                    send_message(sender_id, "No flights were found from " + query.route_from + " to " + query.route_to + " at " +
                                 "{}/{}/{}".format(str(query.day).zfill(2), str(query.month).zfill(2), query.year))
            except:
                traceback.print_exc()
                print("response: " + str(resp))
                send_message(sender_id, "There was an error.")
            del queries[sender_id]
            send_message(sender_id, "Thank you for using our service.")
    elif response is not None:
        send_message(sender_id, response + "\nInformation is needed about: " + query.required_fields())
    else:
        send_message(sender_id, "Information is needed about: " + query.required_fields())


def send_message(recipient_id, message_text):
    data = json.dumps({ "recipient": { "id": recipient_id }, "message": { "text": message_text }})
    headers = { "Content-Type": "application/json" }
    query = urllib.urlencode({ "access_token": access_token })
    conn = httplib.HTTPSConnection("graph.facebook.com")
    conn.request("POST", "/v2.6/me/messages?" + query, data, headers)
    conn.getresponse()
    conn.close()


if __name__ == '__main__':
    app.run(debug=True)
